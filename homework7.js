"use strict";

/*
Практичні завдання 
1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome,
яка приймає рядок str і повертає true, якщо рядок є паліндромом
(читається однаково зліва направо і справа наліво), або false в іншому випадку.
*/
//
// const word = 'Дід';
// const word2 = 'Баба';
//
// function isPalindrome(str) {
//     let reverseWord = '';
//     for (let i = str.length - 1; i >= 0; i--) {
//         reverseWord += str[i];
//     }
//     return word.toUpperCase() === reverseWord.toUpperCase();
// }
//
// console.log(isPalindrome(word));
// console.log(isPalindrome(word2));

/*
2. Створіть функцію, яка перевіряє довжину рядка.
Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true,
якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший.
Ця функція стане в нагоді для валідації форми. Приклади використання функції:

Рядок коротше 20 символів
funcName('checked string', 20); // true
Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false
*/
//
// const userPhrase = prompt('Введіть свій текст:');
//
// function checkStringLength(str, maxLength) {
//     if (str !== null) {
//         return str.length <= maxLength;
//     } else {
//         console.log('Форма не була заповнена')};
// }
//
// console.log(checkStringLength(userPhrase, 10));

/*
3. Створіть функцію, яка визначає скільки повних років користувачу.
Отримайте дату народження користувача через prompt.
Функція повинна повертати значення повних років на дату виклику функцію.
*/

// const userDate = new Date(prompt('Введіть дату народження:', 'YYYY-MM-DD'));
//
// function checkFullYears (date, cb) {
//         return Math.floor((Date.now() - date - cb + (1000 * 60 * 60 * 24)) / 1000 / 60 / 60 / 24 / 365);
//     }
//
// // (1000 * 60 * 60 * 24) - Коригування на ДР, тому що новий повний рік наступає в цей день, а не в наступний
// // Коригування на високосний рік
//
// function isLeap (date) {
//     const dateToday = new Date(Date.now());
//     let leapDays = 0;
//     for (let i = date.getFullYear() ; i <= dateToday.getFullYear() ; i++) {
//         let leap = new Date(i, 1, 29).getDate() === 29;
//         if (leap) {
//             leapDays++;
//         }
//     }
//     return leapDays * 24 * 60 * 60 * 1000;
// }
//
// console.log(checkFullYears(userDate, isLeap(userDate)));

